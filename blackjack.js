const blackjackDeck = deck;

/**
 * Represents a card player (including dealer).
 * @constructor
 * @param {string} name - The name of the player
 */
 class CardPlayer { constructor(name) {
   // CREATE FUNCTION HERE
   this.name = name;
   this.hand=[];
   this.hand=drawCard();
   function drawCard(){
     let card = blackjackDeck[Math.floor(Math.random()*blackjackDeck.length)];
     return card;
   };
 }
}
// CREATE TWO NEW CardPlayers
const playerName = "Player";
const dealerName = "Dealer";
let player= new CardPlayer(playerName);
let dealer= new CardPlayer(dealerName);


/**
 * Calculates the score of a Blackjack hand
 * @param {Array} hand - Array of card objects with val, displayVal, suit properties
 * @returns {Object} blackJackScore
 * @returns {number} blackJackScore.total
 * @returns {boolean} blackJackScore.isSoft
 */
const calcPoints = (hand) => {
  // CREATE FUNCTION HERE
  blackJackScore={
    total: 0,
    isSoft: false,
  };
  for (var i = 0; i < hand.length; i++) {
    if(hand[i].displayVal === "Ace" && hand.filter("Ace") === undefined){
          blackJackScore.total += hand[i].val;
    }
    else{
      total = hand[i] + 1;
    }
  }
  return blackJackScore.total;
}

const calcPoints2 = function(hand) {
  let hasAce = false;
  let handScore = 0;
  let isSoft = false;

  for (let i=0; i< hand.length; i++) {
    const card = hand[i];

    if(card.displayVal === 'Ace'){
      hasAce = true;
      handScore += 1;
    } else {
      handScore += card.val;
    }
  }
  if (handScore <= 11 && hasAce) {
    handScore += 10;
    isSoft = true;
  }
  return {
    total: handScore,
    isSoft: isSoft
  }
}


/**
 * Determines whether the dealer should draw another card.
 *
 * @param {Array} dealerHand Array of card objects with val, displayVal, suit properties
 * @returns {boolean} whether dealer should draw another card
 */
const dealerShouldDraw = (dealerHand) => {
  // CREATE FUNCTION HERE
  let value = dealerHand.calcPoints;
  if(value <16 || value==17 && ace.val ===11){
    dealer.drawCard();
  }
}

/**
 * Determines the winner if both player and dealer stand
 * @param {number} playerScore
 * @param {number} dealerScore
 * @returns {string} Shows the player's score, the dealer's score, and who wins
 */
const determineWinner = (playerScore, dealerScore) => {
  // CREATE FUNCTION HERE
  playerScore = player.calcPoints;
  dealerScore = dealer.calcPoints;
  playerIsWinner = false;
  if(playerScore > dealerScore){
    return playerIsWinner = true;
}

/**
 * Creates user prompt to ask if they'd like to draw a card
 * @param {number} count
 * @param {string} dealerCard
 */
const getMessage = (count, dealerCard) => {
  return `Dealer showing ${dealerCard.displayVal}, your count is ${count}.  Draw card?`
}

/**
 * Logs the player's hand to the console
 * @param {CardPlayer} player
 */
const showHand = (player) => {
  const displayHand = player.hand.map((card) => card.displayVal);
  console.log(`${player.name}'s hand is ${displayHand.join(', ')} (${calcPoints(player.hand).total})`);
}

/**
 * Runs Blackjack Game
 */
const startGame = function() {
  player.drawCard();
  dealer.drawCard();
  player.drawCard();
  dealer.drawCard();

  let playerScore = calcPoints(player.hand).total;
  showHand(player);
  while (playerScore < 21 && confirm(getMessage(playerScore, dealer.hand[0]))) {
    player.drawCard();
    playerScore = calcPoints(player.hand).total;
    showHand(player);
  }
  if (playerScore > 21) {
    return 'You went over 21 - you lose!';
  }
  console.log(`Player stands at ${playerScore}`);

  let dealerScore = calcPoints(dealer.hand).total;
  while (dealerScore < 21 && dealerShouldDraw(dealer.hand)) {
    dealer.drawCard();
    dealerScore = calcPoints(dealer.hand).total;
    showHand(dealer);
  }
  if (dealerScore > 21) {
    return 'Dealer went over 21 - you win!';
  }
  console.log(`Dealer stands at ${dealerScore}`);

  return determineWinner(playerScore, dealerScore);
}
 console.log(startGame());
}
