/**
 * Determines whether meat temperature is high enough
 * @param {string} kind
 * @param {number} internalTemp
 * @param {string} doneness
 * @returns {boolean} isCooked
 */
const foodIsCooked = function(kind, internalTemp, doneness) {
  // Write function HERE
  if (kind.toLowerCase() === 'chicken') {
    if(internalTemp >= 165){
      return doneness='true'
    }
    return doneness='false';
  }
  else if (kind.toLowerCase() === 'beef' && internalTemp >= 125) {
    if(internalTemp <= 135){
      return doneness='true';
    }
    else if (internalTemp >134 && internalTemp<155 && doneness=='rare' || doneness=='medium'){
      return doneness = 'true';
    }
    else{
      return doneness='false';
    }
  }
}



// Test function
console.log(foodIsCooked('chicken', 90)); // should be false
console.log(foodIsCooked('chicken', 190)); // should be true
console.log(foodIsCooked('beef', 138, 'well')); // should be false
console.log(foodIsCooked('beef', 138, 'medium')); // should be true
console.log(foodIsCooked('beef', 138, 'rare')); // should be true
