/**
 * Returns an array of 52 Cards
 * @returns {Array} deck - a deck of cards
 */
const getDeck = () => {
  // Write function HERE
    let cards = [];
    const suits = ['Hearts', 'Spades', 'Clubs', 'Diamonds'];
    const face = ['Jack', 'Queen', 'King'];
    for (let i = 0; i < suits.length; i++) {
      for (let j = 2; j <= 10; j++) {
        cards.push({
          val: j,
          displayVal: j.toString(),
          suit: suits[i],
        });
      }

      }
      for (let x = 0; x < suits.length; x++) {
        for (let i = 0; i < face.length; i++) {
          cards.push({
            val: 10,
            displayVal: face[i].toString(),
                    suit: suits[x],
          })
        }
      }

      for (let i = 0; i < suits.length; i++) {
        cards.push({
          val:11,
          displayVal: 'Ace',
          suit: suits[i],
        })
    }
    return cards;
}



// CHECKS
const deck = getDeck();
console.log(`Deck length equals 52? ${deck.length === 52}`);

const randomCard = deck[Math.floor(Math.random() * 52)];

const cardHasVal = randomCard && randomCard.val && typeof randomCard.val === 'number';
console.log(`Random card has val? ${cardHasVal}`);

const cardHasSuit = randomCard && randomCard.suit && typeof randomCard.suit === 'string';
console.log(`Random card has suit? ${cardHasSuit}`);

const cardHasDisplayVal = randomCard &&
  randomCard.displayVal &&
  typeof randomCard.displayVal === 'string';
console.log(`Random card has display value? ${cardHasDisplayVal}`);
